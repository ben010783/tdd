importScript('app_test:lib/TestSuiteBase.ds');

var Assert = require('/app_test/cartridge/scripts/lib/Assert'),
	CipherHelper = require('app_demo/cartridge/scripts/CipherHelper'),
	expectedCypherText = 'wFKBVVb/9+Xmk2XiR9sJiw==';

function CipherHelperTestSuite() {
	let suite = TestSuiteBase();
	
	//runs before each test
	suite.testSetup = function() {
		Logger.debug('testSetup called');
	};
	
	//runs after each test
	suite.testTearDown = function() {
		Logger.debug('testTearDown called');
	}; 
	
	suite.tests = [{
		'name': 'CipherHelper_encrypt_should_succeed',
		'category': 'Happy',
		'skip': true,
		'run': function () {
			//Arrange
			let stringToEncrpt = 'testing',
				actual;							
			
			//Act	
			actual = CipherHelper.encrypt(stringToEncrpt);
			
			//Assert
			Assert.areEqual(expectedCypherText, actual);
		}
	}, {
		'name': 'CipherHelper_decrypt_should_succeed',
		'category': 'Happy',
		'run': function () {
			//Arrange
			let stringToDecrypt = expectedCypherText,
				expected = 'testing',
				actual;
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt),
			
			//Assert
			Assert.areEqual(expected, actual);
		}
	}, {
		'name': 'CipherHelper_encrypt_null_string_should_return_null_string',
		'category': 'Happy',
		'run': function () {
			//Arrange
			let stringToDecrypt = null,
				expected = null,
				actual;
			
			//Act
			actual = CipherHelper.encrypt(stringToDecrypt)
			
			//Assert
			Assert.areEqual(expected, actual, 'Encrypted string was not null');
		}
	}, {
		'name': 'CipherHelper_encrypt_empty_string_should_return_empty_string',
		'category': 'Happy',
		'run': function () {
			//Arrange
			let stringToDecrypt = '',
				expected = '',
				actual;
				
			//Act
			actual = CipherHelper.encrypt(stringToDecrypt);
			
			//Assert
			Assert.areEqual(expected, actual, 'Encrypted string was not null');
		}
	}, {
		'name': 'CipherHelper_decrypt_null_string_should_return_null_string',
		'category': 'Happy',
		'run': function () {
			//Arrange
			let stringToDecrypt = null,
				expected = null,
				actual;
				
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt),
			
			//Assert	
			Assert.areEqual(expected, actual);
		}
	}, {
		'name': 'CipherHelper_decrypt_empty_string_should_return_empty_string',
		'category': 'Happy',
		'run': function () {
			//Arrange
			let stringToDecrypt = '',
				expected = '',
				actual;
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt),
			
			//Assert
			Assert.areEqual(expected, actual);
		}
	}, {
		'name': 'CipherHelper_encrypt_10kb_string_100_times_should_succeed',
		'category': 'Happy',
		'run': function () {
			//Arrange
			var SecureRandom = require('dw/crypto').SecureRandom;
			
			//Act
			for (let i = 0; i < 100; i++) {
				let secureRandom = new SecureRandom(),
					stringToEncrypt = StringUtils.encodeBase64(secureRandom.nextBytes(10240).toString()); //10kb = 10240 bytes
				CipherHelper.encrypt(stringToEncrypt);
			};
			
			//Assert - No assertions needed since any exception raised will fail this test
		}
	}, {
		'name': 'CipherHelper_decrypt_bad_format_string_should_return_null',
		'category': 'Happy',
		'run': function () {
			//Arrange
			let stringToDecrypt = 'Unencrypted String',
				expected = null,
				actual;
			
			//Act
			actual = CipherHelper.decrypt(stringToDecrypt),
			
			//Assert
			Assert.areEqual(expected, actual);
		}
	}, {
		'name': 'CipherHelper_testing_expected_exception_where_code_does_throw_exception',
		'category': 'Error',
		'expectedError': 'demo exception',
		'run': function () {
			//Arrange
			
			//Act
			CipherHelper.throwException();
			
			//Assert - No assertions needed since 'expectedError' was set
		}
	},{
		'name': 'CipherHelper_testing_expected_exception_where_code_does_not_throw_exception',
		'category': 'Error',
		'expectedError': 'demo exception',
		'run': function () {
			//Arrange
			
			//Act
			Logger.debug('This is an example of a failed test.');
			CipherHelper.doesNotThrowException();
			
			//Assert - No assertions needed since 'expectedError' was set
		}	
	}
	];

	return suite;
};

module.exports = CipherHelperTestSuite();