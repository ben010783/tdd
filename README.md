TDD Framework
==================

If you're interested in writing more robust code you should really consider writing tests for your code. TDD or Test Driven Development is a principle where you write the test first and then write your code.  It's a common practice among professional developers and now you can do TDD on the Demandware platform with the "TDD Framework for Demandware".  

Below are some screen shots.


## MAIN SCREEN


---

![Main Screen](./images/main-screen.png)


## TEST SCREEN


---

![Main Test Screen](./images/main-test-suite.png)
 
## ROAD MAP

* Convert pipelines to controllers.
* Update the XML report to render JUNIT format XML for easy integration with Jenkins. See [Continous Integration](https://bitbucket.org/jmatos/tdd/wiki/ContinuousIntegration.md)

---

## UPDATES:


### Support for BDD style tests (i.e. Mocha) is now included.

Example:

```javascript
var Assert = require('/app_test/cartridge/scripts/lib/Assert');
var bdd = require('/app_test/cartridge/scripts/lib/bdd');
var describe = bdd.describe;
var it = bdd.it; 

module.exports =
describe('CipherHelper', function() {
  var CipherHelper = require('app_demo/cartridge/scripts/CipherHelper'),
      expectedCypherText = 'wFKBVVb/9+Xmk2XiR9sJiw==';

  describe('encrypt', function() {
    it('should succeed when using valid input', function() {
      //Arrange
      let stringToEncrpt = 'testing',
          actual = undefined,
          expected = expectedCypherText;

      //Act
      actual = CipherHelper.encrypt(stringToEncrpt);

      //Assert
      Assert.areEqual(expected, actual);
    });

    it('should return null when input is null', function() {
      //Arrnge
      let stringToDecrypt = null,
          expected = null,
          actual = undefined;

      //Act
      actual = CipherHelper.encrypt(stringToDecrypt);

      //Assert
      Assert.areEqual(expected, actual);
    });
  }
}

```

An example test suite using BDD style.
```/app_demo/cartridge/scripts/tests/CipherHelperBDD.ds```

Support for the legacy style tests is still included except that the built-in Assert function is now a module.

Any test suite that is using the Assert script should be updated so that it uses the "require()" function instead.

Example:

```javascript
var Assert = require('/app_test/cartridge/scripts/lib/Assert');
```

### Continuous Integration

The url used to invoke all the tests of a test suite required 2 parameters __suite__ and __testid__.  Now the __testid__ is optional, and if it is omitted, all the tests in the suite are executed. See [Continous Integration](https://bitbucket.org/jmatos/tdd/wiki/ContinuousIntegration.md)

---

## Contributors
* Gabriel Freiberg
* Karthikeyan Nagendran
* danijel_h

---

[Continue to Wiki](https://bitbucket.org/jmatos/tdd/wiki/Home)
